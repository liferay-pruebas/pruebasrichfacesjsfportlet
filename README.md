# PruebasRichFacesJSF

+ Prueba de formularios con [BeanValidations](https://beanvalidation.org/). **No funcionan en los portlets JSF de Liferay**.
+ Desactivación de estilos automáticos de RichFaces (interfieren con el tema de Liferay) mediante la propiedad *org.richfaces.enableControlSkinning*. Se modifican solo los componentes propios de la librería.
+ Adaptación de una prueba con elementos Drag&Drop de RichFaces.