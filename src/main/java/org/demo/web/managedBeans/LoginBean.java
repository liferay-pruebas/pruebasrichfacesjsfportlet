package org.demo.web.managedBeans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;

/**
 *
 * @author david
 */
@ManagedBean(name = "login")
@RequestScoped
public class LoginBean {

    @Size(min = 5, max = 15, message = "Tama�o incorrecto password")
    private String password;
    
    @Size(min = 5, max = 15, message = "Tama�o incorrecto confirmaci�n")
    private String confirm;
    
    private String status = "";
    
    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    //Validaci�n entre campos, validaci�n cruzada
    //Asegura que este m�todo devuelva true
    //Se ejecuta s� o s�
    @AssertTrue(message = "Las claves son diferentes")
    public boolean arePasswordsEqual(){
        return password.equals(confirm);
    }
    
    public void almacenarNuevaPassword(){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("La nueva clave se ha guardado"));
    }
}