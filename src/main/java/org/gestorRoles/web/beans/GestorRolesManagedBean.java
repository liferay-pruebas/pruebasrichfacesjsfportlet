package org.gestorRoles.web.beans;

import java.io.Serializable;
import java.util.Collection;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.richfaces.event.DropEvent;

/**
 *
 * @author david
 */
@ManagedBean(name = "gestorRoles")
@ViewScoped
public class GestorRolesManagedBean implements Serializable{
    
    private String rolSeleccionado;
    
    @ManagedProperty(value = "#{app}")
    private AppManagedBean servicio;

    /**
     * Creates a new instance of GestorRolesManagedBean
     */
    public GestorRolesManagedBean() {
    }

    public String getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(String rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }
    
    public void setServicio(AppManagedBean servicio) {
		this.servicio = servicio;
	}
    
    public Collection<String> getListaRoles(){
        return servicio.getRoles();
    }
    
    public Collection<String> getListaUsuariosEnRol(){
        if(rolSeleccionado != null)
            return servicio.getListaUsuariosEnRol(rolSeleccionado);
        
        return null;
    }
    
    public Collection<String> getListaUsuariosNoEnRol(){
        if(rolSeleccionado != null)
            return servicio.getListaUsuariosNoEnRol(rolSeleccionado);
        
        return null;
    }
    
    public void dragListenerListaUsuariosNoRol(DropEvent ev){
        System.out.println("Drag de rol a usuario");
        String usuario = (String) ev.getDragValue();
        
        servicio.deleteUsuarioDeGrupo(usuario, rolSeleccionado);
    }
    
    public void dragListenerListaUsuariosEnRol(DropEvent ev){
        System.out.println("Drag de usuario a rol");
        String usuario = (String) ev.getDragValue();
        
        servicio.addUsuarioAGrupo(usuario, rolSeleccionado);
    }
}