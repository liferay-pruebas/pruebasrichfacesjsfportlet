package org.gestorRoles.web.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author david
 */
@ManagedBean(name = "app")
@ApplicationScoped
public class AppManagedBean {
    
    private List<String> roles;
    private List<String> usuarios;
    private Map<String, Set<String>> usuariosRoles;

    /**
     * Creates a new instance of AppManagedBean
     */
    public AppManagedBean() {
        this.roles = new ArrayList<>();
        this.roles.add("ADMIN");
        this.roles.add("RRHH");
        this.roles.add("LOG�STICA");
        this.roles.add("CAMIONEROS");
        
        this.usuarios = new ArrayList<>();
        this.usuarios.add("Pepe");
        this.usuarios.add("Ana");
        this.usuarios.add("Mario");
        this.usuarios.add("Maria");
        this.usuarios.add("Luis");
        this.usuarios.add("David");
        this.usuarios.add("Geralt");
        this.usuarios.add("Gandalf");
        
        this.usuariosRoles = new HashMap<>();
    }

    public List<String> getRoles() {
        return roles;
    }

    public List<String> getUsuarios() {
        return usuarios;
    }

    public Map<String, Set<String>> getUsuariosRoles() {
        return usuariosRoles;
    }
    
    public Collection<String> getListaUsuariosEnRol(String rol) {
        Map<String, Set<String>> mapa = this.getUsuariosRoles();
        if (mapa.containsKey(rol)) {
            return mapa.get(rol);
        }

        return null;
    }

    public Collection<String> getListaUsuariosNoEnRol(String rol) {
        Map<String, Set<String>> mapa = this.getUsuariosRoles();
        if (mapa.containsKey(rol)) {
            List<String> usuariosNoRol = new ArrayList<>();
            for (String u : this.getUsuarios()) {
                if (!mapa.get(rol).contains(u)) {
                    usuariosNoRol.add(u);
                }
            }
            return usuariosNoRol;
        } else {
            return this.getUsuarios();
        }
    }

    public void addUsuarioAGrupo(String usuario, String rol) {
        Map<String, Set<String>> mapa = this.usuariosRoles;
        if (!mapa.containsKey(rol)) {
            mapa.put(rol, new TreeSet<String>());
        }

        mapa.get(rol).add(usuario);
    }

    public void deleteUsuarioDeGrupo(String usuario, String rol) {
        this.usuariosRoles.get(rol).remove(usuario);
    }

}